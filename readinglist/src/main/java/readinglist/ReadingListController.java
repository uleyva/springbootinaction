package readinglist;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class ReadingListController {

	private ReadingListRepository readingListRepository;
	private static final Log LOG = LogFactory.getLog(ReadingListController.class);

	@Autowired
	public ReadingListController (ReadingListRepository readingListRepository) {
		this.readingListRepository = readingListRepository;
	}
	
	@RequestMapping(value = "/{reader}", method = RequestMethod.GET)
//	@GetMapping(value = "/{reader}")
	public String readersBooks(
			@PathVariable("reader") String reader, Model model) {
		LOG.info("readerBooks ok");
		List<Book> readingList = readingListRepository.findByReader(reader);
		if(readingList != null) {
			model.addAttribute("books", readingList);
		}
		return "readingList";
	}
	
	@RequestMapping(value = "/{reader}", method = RequestMethod.POST)
//	@PostMapping(value = "/{reader}")
	public String addToReadingList (
			@PathVariable("reader") String reader, Book book) {
		LOG.info("addreadingList ok");
		String redirect = "redirect:/{reader}";
		book.setReader(reader);
		LOG.info("Book to save-- "+book);
		readingListRepository.save(book);
		return redirect;
		
	}

}
